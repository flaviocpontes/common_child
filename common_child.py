from collections import Counter

def common_child(word1, word2):
    count1 = Counter(word1)
    count2 = Counter(word2)
    common = {char for char in count1.keys() if char in count2.keys()}
    return sum(map(abs, [count1[key]-count2[key] for key in common]))