from unittest import TestCase

from common_child import common_child


class TestCommonChild(TestCase) :
    def test_0(self):
        self.assertEqual(0, common_child("aa", "bb"))
    def test_1(self):
        self.assertEqual(1, common_child("aa", "ab"))
    def test_2(self):
        self.assertEqual(2, common_child("HARRY","SALLY"))
    def test_3(self):
        self.assertEqual(3, common_child("SHINCHAN", "NOHARAAA"))
    def test_same_string(self):
        self.assertEqual(2, common_child("aa", "aa"))


